#!/bin/bash

# Copyright (c) 2020, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:

#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Usage:
# 0) Make sure you have the `jq` tool installed first
# 1) Point this script to a file containing the JSON result of a
#    `tor_dns_survey`-run, optionally specifying an error threshold for relays
#    we are interested in (default is 10)
# 2) The result will be the RSA fingerprints of relays with at least as many
#    test run errors shown as the chosen threshold specifies.

if [ "$#" = 1 ]; then
  json_file="$1"
  threshold=10
elif [ "$#" = 2 ]; then
  json_file="$1"
  threshold="$2"
else
  echo >&2 "Usage: $0 JSON_file [error threshold]"
  exit 1
fi

date=$(date +%s)
# Extract the relays and fingerprints
relays=$(jq '.["eff.org"]' "$json_file")
fps=$(jq keys <<< "$relays")
# How many relays we tested
relay_length=$(jq length <<< "$fps")
for (( r = 0; r < relay_length; r++ ))
do
  failures=0
  status=()
  relay=$(jq .[$r] <<< "$fps")
  echo "Relay to check: $relay"
  relay_results=$(jq .["$relay"] <<< "$relays")
  # We have measured each relay ten times, let's check the results
  for i in {0..9}
  do
    result=$(jq --arg count "$i" '.[$count|tonumber] | .[0]' <<< "$relay_results")
    # Record the status, so we can help relay operators better
    status+=("$result")
    if [[ $result != *"\"SUCCEEDED\""* ]]
    then
      ((failures++))
    fi
  done
  # We are only interested in relays with failures that are equal to or above our
  # threshold.
  if [[ $failures -ge $threshold ]]
  then
    echo "$relay" | grep -oh -E "[0-9A-F]{40}" >> dns_failures_"$date"
    for i in "${status[@]}"
    do
      echo "  $i" >> dns_failures_"$date"
    done
    echo "" >> dns_failures_"$date"
  fi
done
exit 0
