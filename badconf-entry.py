#!/usr/bin/env python3
#
# Copyright (c) 2020, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
Usage:
1) Make sure you are in the dirauth-conf repository when invoking this script.
2) It takes a file containing RSA-key related fingerprints as an argument.
   Anything else is not supported right now. Make sure the fingerprints to
   reject are not already rejected first, e.g. by using the fp-match-script.sh
   shell script.
"""

import hashlib
import os
import random
import re
import readline
import sys
import textwrap

from datetime import datetime, timedelta
from email.utils import formatdate, parsedate_to_datetime, format_datetime
from email.utils import parseaddr

from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.remote import DescriptorDownloader

from util import load_args_as_fp

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

# Fingerprints to test.
fps = []
# Fingerprints still found in the consensus.
fps_in_consensus = {}

comment_template_reject = """
# Identifier: %s
# Reported-by: %s
# Date: %s
# Expire: %s
# MessageID: %s
# Reason: %s
"""

comment_template_auth_dir_reject = """
# Identifier: %s
# Reported-by: %s
# Date: %s
# Expire: %s
# MessageID: %s
# Reason: %s
# Fingerprints: %s
"""

# Marker in the comment if the field doesn't have any value(s).
none_marker = "<none>"

def ASK(msg):
    """ Ask something to the user using input(). """
    return input("%s: " % (msg)).strip()

def ERR(msg):
    """ Print standard error message. """
    print("[ERROR] %s" % (msg), file=sys.stderr)

def ask_reported_by():
    """
    Ask the user who reported the rule. This is a mandatory field with no
    default value.
    """
    while True:
        reporter = ASK("Reported by, the name is optional (Full Name <user_email@foo.bar>)")
        try:
            name, email = parseaddr(reporter)
        except ValueError as e:
            print("Invalid format: %s" % (reporter))
            continue
        if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
            ERR("Invalid email address: %s" % (email))
            continue
        return "%s <%s>" % (name, email)

def ask_date():
    """
    Ask the user when this bad relay(s) was found. This is mandatory and the
    default value is the current date. The user can use localtime or UTC but
    needs to specify the timezone offset if NOT UTC. Default is printed as
    localtime and then changed to UTC.
    """
    while True:
        utc_now = formatdate()
        date = ASK("When was the relay found? [%s]" % formatdate(localtime=True))
        if date == "":
            return utc_now
        # Is date well formatted?
        try:
            dt = parsedate_to_datetime(date)
        except TypeError as e:
            ERR("Invalid date: %s" % (date))
            ERR("Expected date format (%a, %d %b %Y %H:%M:%S %z)")
            ERR("Ex: Sat, 26 Oct 1985 09:00:00 +0400")
            continue
        # Convert it to UTC. Tell the datetime person that the API is horrible.
        utc = dt - dt.utcoffset()
        utc = utc.replace(tzinfo=None)
        return format_datetime(utc)

def ask_expiry():
    """
    Ask the user for an expiry time of the entry. This is mandatory and there
    is a default value.
    """
    while True:
        # 3 months default.
        default = 3 * 30
        expiry = ASK("Entry expiration (number of days) [%d]" % (default))
        if expiry == "":
           expiry = default
        else:
            try:
                int(expiry)
            except ValueError as e:
                ERR("Invalid expiry value: %s" % (expiry))
                continue
        dt = datetime.utcnow() + timedelta(days = int(expiry))
        return dt.strftime("%Y-%m-%d")

def ask_message_id():
    """ This is not mandatory. """
    msg_id = ASK("Message ID of bad-relays@ thread (optional)")
    if msg_id == "":
        msg_id = none_marker
    return msg_id

def ask_reason():
    """
    Ask a reason to the user. This is a custom way to input the user data.
    since we accept multiple lines.
    """
    while True:
        lines = []
        previous_line_empty = False
        print("Reason for entry (press Enter twice to end):")
        for l in sys.stdin:
            line = l.strip()
            if line == "":
                if previous_line_empty is True:
                    break
                previous_line_empty = True
            else:
                previous_line_empty = False
                lines.append(line)

        if len(lines) == 0:
            ERR("A reason MUST be provided.")
            continue
        reason = "\n".join(lines)
        indent = "#%s" % (" " * (len("# Reason: ") - 1))
        tw = textwrap.TextWrapper(width = 76, initial_indent = indent, \
                subsequent_indent = indent, break_long_words = True, \
                replace_whitespace = True)
        # Remove leading indent of the first line.
        return tw.fill(reason)[len(indent):]

def get_fps_in_consensus():
    """
    We want to get the fingerprints that are still in the consensus to be able
    to construct the respective AuthDirRejectLines
    """
    for fp, desc in rse.routers.items():
        if fp in fps:
            fps_in_consensus[fp] = desc
    return fps_in_consensus

def main():
    """ Entry point of script. """
    # Load arguments and consider them as fingerprints which are put in fps.
    load_args_as_fp(sys.argv, fps)
    if len(fps) == 0:
        # Nothing to do here.
        print("Nothing to do, exiting...")
        return
    # Get the person who reported the bad relay(s).
    reported_by = ask_reported_by()
    # Get the date the bad relay(s) was/were found.
    date = ask_date()
    # Get expiry time.
    expiry = ask_expiry()
    # Get message ID of bad-relays@ email if available.
    message_id = ask_message_id()
    # Get the reason for the entry.
    reason = ask_reason()
    # Gets the fingerprints still in the consensus for AuthDirReject entries
    fps_in_consensus = get_fps_in_consensus()
    # Create the identifier
    payload = "identifier-%.10f-%s-%s-%s" % (random.random(), reported_by, date, reason)
    identifier = hashlib.sha1(payload.encode()).hexdigest()[:16]

    # Add AuthDirReject lines if there are any and create fingerprints comment
    # if needed
    rules = []
    fingerprints = []
    fingerprints_in_comment = None
    if len(fps_in_consensus) > 0:
        for desc in fps_in_consensus.values():
            rules.append("AuthDirReject %s\n" % (desc.address))
        for fp in sorted(fps_in_consensus):
            fingerprints.append(fp)
        indent = "#%s" % (" " * (len("# Fingerprints: ") - 1))
        tw = textwrap.TextWrapper(width = 76, \
                initial_indent = indent, subsequent_indent = indent)
        # Remove leading indent of the first line.
        fingerprints_in_comment = tw.fill(", ".join(fingerprints))[len(indent):]
        with open("torrc.d/bad.conf", 'a') as bad_conf:
            bad_conf.write(comment_template_auth_dir_reject % \
                (identifier, reported_by, date, expiry, message_id, reason,
                 fingerprints_in_comment))
            for rule in sorted(set(rules)):
                bad_conf.write(rule)
    else:
        print("No AuthDirReject rules to create...")
        print("Continuing with !reject rules.")

    with open("approved-routers.d/approved-routers.conf", 'a') as routers_conf:
        routers_conf.write(comment_template_reject % \
            (identifier, reported_by, date, expiry, message_id, reason))
        for fp in fps:
            routers_conf.write("!reject %s\n" % fp)

if __name__ == '__main__':
    main()
