#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2020, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys, os
import stem

from datetime import datetime
from stem import Flag
from stem.descriptor import DocumentHandler, parse_file
from stem.descriptor.remote import DescriptorDownloader

from util import load_args_as_fp

# Fingerprints to test.
fps = []
ed25519_id = {}
total_bw = 0.0
guard_count = 0
hsdir_count = 0
exit_count = 0

# Load arguments and consider them as fingerprint which are put in fps.
load_args_as_fp(sys.argv, fps)

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

print("[+] Testing %d relays..." % (len(fps)))

found = {}
print("\n== In Consensus ==")
for fp, desc in rse.routers.items():
    if fp in fps:
        if Flag.BADEXIT in desc.flags:
            print("  [-] (BADEXIT) %s" % (fp))
        else:
            found[fp] = desc
        sd = get_sd(desc)
        ed25519_id[fp] = sd.ed25519_master_key
        # Print useful info.
        describe(desc)
        if sd is not None:
            total_bw += sd.observed_bandwidth
        if Flag.GUARD in desc.flags:
            guard_count += 1
        if Flag.HSDIR in desc.flags:
            hsdir_count += 1
        if Flag.EXIT in desc.flags:
            exit_count += 1

print("[+] %d/%d in consensus" % (len(found), len(fps)))
print("   > Guard: %d" % (guard_count))
print("   > HSDir: %d" % (hsdir_count))
print("   > Exit: %d" % (exit_count))

print("\n== NOT In Consensus ==")
not_blocked = list(set(fps) - set(found))
for fp in not_blocked:
    print("  [+] %s" % (fp))

print("[+] %d/%d are not in the consensus" % (len(not_blocked), len(fps)))

print("\n== Fingerprints ==")
sort_found = sorted(found)
fp_str = ""
for fp in sort_found:
    fp_str += "%s," % (fp)
print(fp_str[:-1])

print("\n== Rules ==")
rules = []
for desc in found.values():
    rules.append("AuthDirReject %s" % (desc.address))
for rule in sorted(set(rules)):
    print(rule)

print("\n== Approved Routers ==")
rules = []
for desc in found.values():
    rules.append("!reject %s" % (desc.fingerprint))
for rule in sorted(set(rules)):
    print(rule)

print("\n== Approved Routers ==")
rules = []
for fp in sort_found:
    rules.append("!reject %s" % (ed25519_id[fp]))
for rule in sorted(set(rules)):
    print(rule)

print("\n== Total Bandwidth ==")
print("[+] %f MB/s" % (total_bw / 1000.0 / 1000.0))
