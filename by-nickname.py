#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2017, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import sys

# Fingerprint to fetch
try:
    nickname = sys.argv[1]
except IndexError as ie:
    print("[-] Missing Nickname")
    sys.exit(1)

print("[+] Searching for nickname containing \"%s\"... " % (nickname))

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

entries = []

# Get server descriptor.
for fp, desc in rse.routers.items():
    sd = get_sd(desc)
    if sd is None:
        continue
    if nickname in sd.nickname:
        entries.append(desc)

print("[+] Found %d relays" % (len(entries)))

for desc in entries:
    describe(desc)

print("\n== Fingerprints ==")
# Print fingerprints.
for desc in entries:
    print("%s," % (desc.fingerprint), end="")

# Print dirauth rules
print("\n\n== Rules ==")
rules = []
for desc in entries:
    rules.append("AuthDirReject %s" % (desc.address))
for rule in sorted(set(rules)):
    print(rule)

print("\n== Approved Routers ==")
rules = []
for desc in entries:
    rules.append("!reject %s" % (desc.fingerprint))
for rule in sorted(set(rules)):
    print(rule)
