#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2019, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import math

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

from collections import defaultdict

per_bw = []

for fp, desc in rse.routers.items():
    if Flag.RUNNING not in desc.flags:
        continue
    sd = get_sd(desc)
    if sd is None:
        continue
    if sd.contact is None:
        continue
    contact = sd.contact.decode('utf-8')
    version = str(desc.version)
    major = version[:5]
    if "0.2." in version and not "0.2.9." in version:
        per_bw.append({'bw': desc.bandwidth, 'fp': desc.fingerprint,
            'nickname': desc.nickname, 'contact': contact, 'version': version})
    if "0.3." in version and not "0.3.4." in version and not "0.3.5." in version:
        per_bw.append({'bw': desc.bandwidth, 'fp': desc.fingerprint,
            'nickname': desc.nickname, 'contact': contact, 'version': version})

for v in sorted(per_bw, key = lambda i: i['bw'], reverse=True):
    print("[bw: %d] [v: %s] [n: %s] Contact: %s - http://rougmnvswfsmd4dq.onion/rs.html#search/%s" % \
          (v['bw'], v['version'], v['nickname'], v['contact'], v['fp']))

