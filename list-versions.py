#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2019, The Tor Project, Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
#     * Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#     * Redistributions in binary form must reproduce the above
# copyright notice, this list of conditions and the following disclaimer
# in the documentation and/or other materials provided with the
# distribution.
#
#     * Neither the names of the copyright owners nor the names of its
# contributors may be used to endorse or promote products derived from
# this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import math

# This will load all needed documents either from the cache in /tmp or
# download them from the dirauth.
from base import *

versions = {}
weights = {}

total_w = 0.0

total_02 = 0
weigth_02 = 0.0

total_27841 = 0
weigth_27841 = 0.0

print("\n== Relays == ")

for fp, desc in rse.routers.items():
  if Flag.RUNNING not in desc.flags:
    continue
  version = str(desc.version)
  major = version[:5]
  if version not in versions:
    versions[version] = 0
    weights[version] = 0
  if major not in versions:
    versions[major] = 0
    weights[major] = 0
  versions[version] += 1
  weights[version] += desc.bandwidth
  versions[major] += 1
  weights[major] += desc.bandwidth
  total_w += desc.bandwidth
  if "0.2." in version and not "0.2.9." in version:
      total_02 += 1
      weigth_02 += desc.bandwidth
  #if "0.3." in version and not "0.3.4." in version and not "0.3.5." in version:
  if "0.3." in version and not "0.3.5." in version:
      total_27841 += 1
      weigth_27841 += desc.bandwidth

for v in sorted(versions.keys()):
  digits = int(math.log10(versions[v])) + 1
  if digits == 4:
    spaces = ""
  elif digits == 3:
    spaces = " "
  elif digits == 2:
    spaces = "  "
  else:
    spaces = "   "

  w_percent = (weights[v] / total_w) * 100

  print("%s%d: %s%s[%.2f %%] %s" % \
        (spaces, versions[v], v, (18 - len(v.__str__())) * " ", w_percent,
          ("(MAJOR)" if len(v) == 5 else "")))

print("\n== Bridges == ")

bridge_versions = {}
bridge_weights = {}
bridge_total_02 = 0
bridge_total_03 = 0
bridge_w02 = 0.0
bridge_w03 = 0.0
bridge_total_w = 0.0

for desc in bds.values():
  version = str(desc.tor_version)
  major = version[:5]
  if version not in bridge_versions:
    bridge_versions[version] = 0
    bridge_weights[version] = 0
  if major not in bridge_versions:
    bridge_versions[major] = 0
    bridge_weights[major] = 0
  bridge_versions[version] += 1
  bridge_weights[version] += desc.observed_bandwidth
  bridge_versions[major] += 1
  bridge_weights[major] += desc.observed_bandwidth
  bridge_total_w += desc.observed_bandwidth
  if "0.2." in version and not "0.2.9." in version:
    bridge_total_02 += 1
    bridge_w02 += desc.observed_bandwidth
  #if "0.3." in version and not "0.3.4." in version and not "0.3.5." in version:
  if "0.3." in version and not "0.3.5." in version:
    bridge_total_03 += 1
    bridge_w03 += desc.observed_bandwidth

for v in sorted(bridge_versions.keys()):
  digits = int(math.log10(bridge_versions[v])) + 1
  if digits == 4:
    spaces = ""
  elif digits == 3:
    spaces = " "
  elif digits == 2:
    spaces = "  "
  else:
    spaces = "   "

  w_percent = (bridge_weights[v] / bridge_total_w) * 100

  print("%s%d: %s%s[%.2f %%] %s" % \
        (spaces, bridge_versions[v], v, (18 - len(v.__str__())) * " ",
         w_percent, ("(MAJOR)" if len(v) == 5 else "")))

print("\n== Relays - Series == ")
print("<= 0.2.8 series: %d [w: %.2f%%]" % (total_02, (weigth_02 / total_w) * 100))
print("0.3.0x - 0.3.4.x series: %d [w: %.2f%%]" % (total_27841, (weigth_27841 / total_w) * 100))

print("\n== Bridge - Series == ")
print("<= 0.2.8 series: %d [w: %.2f%%]" % (bridge_total_02, (bridge_w02 / bridge_total_w) * 100))
print("0.3.0x - 0.3.4.x series: %d [w: %.2f%%]" % (bridge_total_03, (bridge_w03 / bridge_total_w) * 100))
